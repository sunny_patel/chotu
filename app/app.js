import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight
} from 'react-native';
import Drawer from 'react-native-drawer';
import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import { Provider } from 'react-redux';
import FirebaseApi from './lib/FirebaseApi';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import reducer from './reducers';
import AppContainer from './containers/AppContainer';
import Immutable from 'immutable';
import * as firebase from 'firebase';

function configureStore(initialState) {
  const enhancer = compose(
    applyMiddleware(
      thunkMiddleware, // lets us dispatch() functions
      createLogger({
        predicate: (getState, action) => __DEV__,
        stateTransformer: (state) => {
          let newState = {};
          for (var k in state) {
            if (typeof state[k].toJS === 'function') {
              newState[k] = state[k].toJS();
            } else {
              newState[k] = state[k];
            }
          }
          // newState = {...state}
          return newState;
        }
      })
    ),
  );
  return createStore(reducer, initialState, enhancer);
}

export default class app extends Component {
  constructor(props) {
      super(props);
      // Default state
      var init = {};
      // Initialize Firebase
      const firebaseConfig = {
        apiKey: "AIzaSyD0DK7XFNv3qtGkWhWK7VdySVm4Hq9ktaw",
        authDomain: "chotu-a21e6.firebaseapp.com",
        databaseURL: "https://chotu-a21e6.firebaseio.com",
        storageBucket: "chotu-a21e6.appspot.com",
        messagingSenderId: "382236783014"
      };
      firebase.initializeApp(firebaseConfig);

      this.store = configureStore(init);
  };

  render() {
    return (
      <Provider store={this.store}>
        <AppContainer/>
      </Provider>
    )
  };
}
