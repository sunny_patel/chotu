import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TextInput,
  Button
} from 'react-native';
import actions from '../actions';
import utils from '../lib/utils';

class Loading extends Component {
  constructor(props) {
    super(props);
  };

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.msg}>Loading</Text>
      </View>
    )
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#009688',
    padding: 10
  },
  msg: {
    fontSize: 20,
    textAlign: 'center',
    color: '#ffffff'
  }
});

export default Loading;
