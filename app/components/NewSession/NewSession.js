import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ScrollView,
  View,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import {
  Button,
  Grid, Row, Col
} from 'react-native-elements';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import actions from '../../actions/index';
import { ActionCreators } from '../../actions/index';
import CalendarPicker from 'react-native-calendar-picker';
import Moment from 'moment';

import Calendar from '../Calendar';
import DateTimeDisplay from './DateTimeDisplay';
import SceneHeader from '../../containers/DrawerContainer/SceneHeader';

import utils from '../../lib/utils';

const colors = utils.colors.midnightBlue;

class NewSession extends Component {

  constructor(props) {
    super(props);
    var now = new Moment();
    var later = new Moment();
    later.add(6, 'h');
    this.state = {
      openCalendar: false,
      startDate: now,
      endDate: later
    };

    this.pickADate = this.pickADate.bind(this);
    this.onCalendarDateChange = this.onCalendarDateChange.bind(this);
    this.onCalendarDonePressed = this.onCalendarDonePressed.bind(this);
    this.showCalendar = this.showCalendar.bind(this)
  };

  onCalendarDateChange(date) {
    var newState = {
      ...this.state,
    };
    newState[this.state.calendarKey] = date
    this.setState(newState);
  }

  pickADate (key) {
    this.setState({
      ...this.state,
      openCalendar: true,
      calendarKey: key
    });
  }

  onCalendarDonePressed(date, key) {
    this.setState({
      ...this.state,
      openCalendar: false
    });
  }

  showCalendar(){
    if (this.state.openCalendar) {
      return (
        <Calendar
          onDonePress={this.onCalendarDonePressed}
          onDateChange={this.onCalendarDateChange}
          date={this.state[this.state.calendarKey]}
        />
      )
    }
    else {
      return null
    }
  }
  render() {
    if (this.state.openCalendar) {
      return (
        <View style={styles.fullScreen}>
          <this.showCalendar/>
        </View>
      )
    } else {
      return (
        <View style={styles.fullScreen}>
          <View style={styles.sceneHeader}>
            <SceneHeader
              sceneTitle='NEW BOOKING'
              navigation={this.props.navigation}
            />
          </View>

          <ScrollView style={styles.container}>
            <Grid style={{flex:1, padding: 10}}>
              <Row style={{marginBottom: 5}}>
                <Col style={{margin: 5}}>
                  <TouchableOpacity onPress={()=>{this.pickADate('startDate')}}>
                    <DateTimeDisplay title="START" date={this.state.startDate}/>
                  </TouchableOpacity>
                </Col>
                <Col style={{margin: 5}}>
                  <TouchableOpacity onPress={()=>{this.pickADate('endDate')}}>
                    <DateTimeDisplay title="END" date={this.state.endDate}/>
                  </TouchableOpacity>
                </Col>
              </Row>
              <Row style={{marginTop: 5, marginBottom: 5}}>
                <Col>
                </Col>
              </Row>
            </Grid>
          </ScrollView>
        </View>
      )
    }
  };
};

const styles = StyleSheet.create({
  fullScreen: {
    flex: 1
  },
  container: {
    flex: .9,
    backgroundColor: colors.lightestWhite,
    padding: 10
  },
  sceneHeader: {
    flex: .1
  },
  button: {
    borderRadius: 40,
    backgroundColor: 'transparent',
    borderColor: colors.darkestPrimary,
    borderWidth: 1
  },
  buttonText: {
    color: colors.darkestPrimary
  },
  header: {
    color: colors.darkestPrimary,
    fontSize: 24
  },
  row: {
    padding: 5
  },
  col: {
    margin: 10
  },
  msg: {
    fontSize: 20,
    textAlign: 'center',
    color: '#b4bbc5'
  },
});

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    myNavigation: state.navigation.toJS()
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(NewSession);
