import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  ScrollView,
  View,
  Dimensions,
  TouchableOpacity
} from 'react-native';
import {
  Button,
  Grid, Row, Col
} from 'react-native-elements';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import actions from '../../actions/index';
import { ActionCreators } from '../../actions/index';
import CalendarPicker from 'react-native-calendar-picker';
import Moment from 'moment';

import Calendar from '../Calendar';
import SceneHeader from '../../containers/DrawerContainer/SceneHeader';

import utils from '../../lib/utils';

const colors = utils.colors.midnightBlue;

class DateTimeDisplay extends Component {
  constructor(props) {
    super(props);

    this.state = {
      title: this.props.title,
      date: this.props.date,
    };

  };

  render() {
    return (
      <View style={styles.container}>
        <Grid>
          <Row>
            <Col>
              <Text style={styles.title}>{this.state.title}</Text>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.dateText}>{this.state.date.format('ddd, MMM DD, YYYY')}</Text>
            </Col>
          </Row>
          <Row>
            <Col>
              <Text style={styles.timeText}>{this.state.date.format('h:mm a')}</Text>
            </Col>
          </Row>
        </Grid>
      </View>
    )
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    borderRadius: 10,
    borderColor: colors.secondary,
    borderWidth: 1,
    padding: 10,
  },
  title: {
    fontSize: 11,

  },
  dateText: {
    marginTop: 10
  },
  timeText: {
    fontSize: 22
  },
  button: {
    borderRadius: 40,
    backgroundColor: 'transparent',
    borderColor: colors.darkestPrimary,
    borderWidth: 1
  },
  buttonText: {
    color: colors.darkestPrimary
  },
  header: {
    color: colors.darkestPrimary,
    fontSize: 24
  },
  row: {
    padding: 5
  },
  col: {
    margin: 10
  },
  msg: {
    fontSize: 20,
    textAlign: 'center',
    color: '#b4bbc5'
  },
});

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    navigation: state.navigation.toJS()
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(DateTimeDisplay);
