import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TextInput,
  Button
} from 'react-native';
import actions from '../actions';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../actions/index';
import { connect } from 'react-redux'
import utils from '../lib/utils';

class SignupForm extends Component {
  constructor(props) {
      super(props);
      this.state = {
        user: {
          firstName: "",
          lastName: "",
          email: ""
        }
      };
  };

  emailAddrTextChange(text) {
    console.debug("SignupForm : emailAddrTextChange("+text+")");
    if (utils.validEmail(text)) {
      this.updateUserObj('email', text);
    } else {
      console.debug("SignupForm : emailAddrTextChange : invalid email > " + text);
    }
  }

  updateUserObj(fieldToUpdate, valueToUpdate) {
    valueToUpdate = valueToUpdate.trim();
    var user = {...this.state.user, [fieldToUpdate]: valueToUpdate};
    this.setState({user: user});
  }

  onSignupFormSubmit(user) {
    this.props.createFirebaseUser(user)
  };

  render() {
    return (
      <View style={styles.container}>
        <Text>Sign up</Text>
        <TextInput
          style={styles.textInput}
          placeholder={'First Name'}
          keyboardType='ascii-capable'
          autoCorrect={false}
          onChangeText={(text) => {
            this.updateUserObj('firstName', text)
          }}
        />
        <TextInput
          style={styles.textInput}
          placeholder={'Last Name'}
          keyboardType='ascii-capable'
          autoCorrect={false}
          onChangeText={(text) => {
            this.updateUserObj('lastName', text)
          }}
        />
        <TextInput
          style={styles.textInput}
          placeholder={'Phone'}
          keyboardType='phone-pad'
          autoCorrect={false}
          onChangeText={(text) => {
            this.updateUserObj('phone', text)
          }}
        />
        <TextInput
          style={styles.textInput}
          placeholder={'youremail@example.com'}
          keyboardType='email-address'
          autoCorrect={false}
          autoCapitalize="none"
          onChangeText={(text) => {
            this.emailAddrTextChange(text)
          }}
        />
        <TextInput
          style={styles.textInput}
          placeholder={'Password'}
          autoCorrect={true}
          autoCapitalize="none"
          onChangeText={(text) => {
            this.updateUserObj('password', text)
          }}
        />
        <Button
          onPress={() => {
            this.onSignupFormSubmit(this.state.user)
          }}
          title="Submit"
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
        ></Button>
      </View>
    )
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: '#009688',
    padding: 10
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  textInput: {
    backgroundColor: 'white',
    borderWidth: 1,
    borderColor: 'blue',
    height: 40,
    padding: 10
  }
});

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    user: state.user.toJS()
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(SignupForm);
