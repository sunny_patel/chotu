import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput,
  ScrollView,
  Image,
  TouchableOpacity
} from 'react-native';
import {
  Button,
  FormLabel,
  FormInput,
  Grid, Col, Row,
  Icon
} from 'react-native-elements';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../actions/index';
import TextField  from './TextField';
import utils from '../lib/utils';
import SceneHeader from '../containers/DrawerContainer/SceneHeader';

const colors = utils.colors.midnightBlue;


class EditProfile extends Component {
  constructor(props) {
    super(props);

    this.save = this.save.bind(this);
  };

  save () {
    // poor coding but need to get MVP done. should change at some point
    email = this.refs.email.refs.email._lastNativeText !== undefined ?
      this.refs.email.refs.email._lastNativeText.trim() : this.props.user.profile.email
    fullName = this.refs.fullName.refs.fullName._lastNativeText !== undefined ?
      this.refs.fullName.refs.fullName._lastNativeText.trim() : this.props.user.profile.fullName

    updatedProfile = Object.assign({},
      this.props.user.profile,
      {
        email: email,
        fullName: fullName
      }
    )

    this.props.saveUserProfileOnFirebase(updatedProfile);
  }

  render() {
    var saveIcon = (
      <Icon
        raised
        reverse
        name='check'
        type='material-community'
        color={colors.highlightPrimary}
        onPress={this.save}
        buttonStyle={styles.submitBtn}
      />
    );

    var saveBtn = (
      <Button
        title='SAVE'
        backgroundColor={colors.highlightPrimary}
        color={colors.lightestWhite}
        onPress={this.save}
        buttonStyle={styles.submitBtn}
      />
    )
    return (
      <View style={{flex:1}}>
        <View style={{flex: .1}}>
          <SceneHeader
            sceneTitle=''
            navigation={this.props.navigation}
            />
        </View>
        <View style={styles.container}>
          <View style={{justifyContent: 'center', flex: .35, alignItems: 'center', marginBottom: 50}}>
              <TouchableOpacity>
                <Image
                  style={{width: 150, height: 150, borderRadius: 75, borderWidth: 3, borderColor: colors.highlightPrimary, }}
                  source={{uri: 'https://placehold.it/150x150'}}
                />
              </TouchableOpacity>
          </View>
          <ScrollView style={{flex: 1}}>
            <Grid>
              <Row size={1}>
                <Col>
                  <FormLabel
                    labelStyle={styles.formLabel}
                  >Name</FormLabel>
                  <FormInput
                    onChangeText={this.onChangeText}
                    ref='fullName'
                    textInputRef='fullName'
                    placeholder='e.g. John Doe'
                    placeholderTextColor={colors.darkWhite}
                    autoCorrect={false}
                    autoCapitalize='none'
                    inputStyle={styles.textInput}
                    defaultValue={this.props.user.profile.fullName}
                  />
                </Col>
              </Row>
              <Row size={1}>
                <Col>
                  <FormLabel
                    labelStyle={styles.formLabel}
                  >Email</FormLabel>
                  <FormInput
                    onChangeText={this.onChangeText}
                    ref='email'
                    textInputRef='email'
                    placeholder='example@gmail.com'
                    placeholderTextColor={colors.darkWhite}
                    autoCorrect={false}
                    autoCapitalize='none'
                    inputStyle={styles.textInput}
                    defaultValue={this.props.user.profile.email}
                    keyboardType='email-address'
                  />
                </Col>
              </Row>
              <Row size={1}>
                <Col>
                  <FormLabel
                    labelStyle={styles.formLabel}
                  >Address</FormLabel>
                  <FormInput
                    onChangeText={this.onChangeText}
                    ref='address'
                    textInputRef='address'
                    placeholder='4 Privet Drive '
                    placeholderTextColor={colors.darkWhite}
                    autoCorrect={false}
                    autoCapitalize='none'
                    inputStyle={styles.textInput}
                    defaultValue={this.props.user.profile.address}
                  />
                </Col>
              </Row>
              <Row size={1}>
                <Col>
                  <FormLabel
                    labelStyle={styles.formLabel}
                  >Address</FormLabel>
                  <FormInput
                    onChangeText={this.onChangeText}
                    ref='address'
                    textInputRef='address'
                    placeholder='4 Privet Drive '
                    placeholderTextColor={colors.darkWhite}
                    autoCorrect={false}
                    autoCapitalize='none'
                    inputStyle={styles.textInput}
                    defaultValue={this.props.user.profile.address}
                  />
                </Col>
              </Row>
              <Row size={1}>
                <Col>
                  <FormLabel
                    labelStyle={styles.formLabel}
                  >Address</FormLabel>
                  <FormInput
                    onChangeText={this.onChangeText}
                    ref='address'
                    textInputRef='address'
                    placeholder='4 Privet Drive '
                    placeholderTextColor={colors.darkWhite}
                    autoCorrect={false}
                    autoCapitalize='none'
                    inputStyle={styles.textInput}
                    defaultValue={this.props.user.profile.address}
                  />
                </Col>
              </Row>
              <Row size={1}>
                <Col>
                  <FormLabel
                    labelStyle={styles.formLabel}
                  >Address</FormLabel>
                  <FormInput
                    onChangeText={this.onChangeText}
                    ref='address'
                    textInputRef='address'
                    placeholder='4 Privet Drive '
                    placeholderTextColor={colors.darkWhite}
                    autoCorrect={false}
                    autoCapitalize='none'
                    inputStyle={styles.textInput}
                    defaultValue={this.props.user.profile.address}
                  />
                </Col>
              </Row>
            </Grid>
          </ScrollView>
          <Grid style={{flex: .25}}>
            <Row>
              <Col>
                {saveBtn}
              </Col>
            </Row>
          </Grid>
        </View>
      </View>
    )
  };
};

const styles = StyleSheet.create({
  container: {
    flex: .9,
    justifyContent: 'space-between',
    flexDirection: 'column',
    backgroundColor: colors.lightestWhite,
  },
  textInput: {
    height: 60
  },
  msg: {
    fontSize: 20,
    textAlign: 'center',
    color: '#FFFFFF'
  },
  textField: {
    height: 50,
    backgroundColor: colors.lightPrimary,
    padding: 10,
    borderColor: colors.lightSecondary,
    borderWidth: 2,
    borderRadius: 3,
    color: colors.darkWhite
  },
  formLabel: {
    color: colors.highlightPrimary
  },
  submitBtn: {
    margin: 20
  }
});

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    myNavigation: state.navigation.toJS(),
    user: state.user.toJS()
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(EditProfile);
