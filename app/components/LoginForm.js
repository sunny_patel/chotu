import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TextInput,
} from 'react-native';
import {
  FormLabel,
  FormInput,
  Button
 } from 'react-native-elements';
import Loading from './Loading';
import actions from '../actions';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../actions/index';
import { connect } from 'react-redux'
import utils from '../lib/utils';

class LoginForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      auth: {
        email: "",
        password: ""
      }
    };
    this.dummyAcctLogin = this.dummyAcctLogin.bind(this);
    this.updateAuthObj = this.updateAuthObj.bind(this);
  };

  emailAddrTextChange(text) {
    console.debug("SignupForm : emailAddrTextChange("+text+")");
    if (utils.validEmail(text)) {
      this.updateAuthObj('email', text);
    } else {
      console.debug("SignupForm : emailAddrTextChange : invalid email > " + text);
    }
  }

  updateAuthObj(fieldToUpdate, valueToUpdate, cb=()=>{}) {
    valueToUpdate = valueToUpdate.trim();
    var auth = Object.assign({}, this.state.auth, {[fieldToUpdate]: valueToUpdate});
    this.setState({auth: auth}, cb)
    console.log({auth: auth});
  }

  submitLoginForm(auth=this.state.auth) {
    this.props.authenicateUser(auth)
  };

  dummyAcctLogin() {
    this.setState({auth: {
      email: 'du@g.com',
      password: 'testtest'
    }}, this.submitLoginForm);

  }

  componentDidMount() {
    // this.dummyAcctLogin()
  }

  render() {
    return (
      <View style={styles.container}>
        <FormLabel>Email</FormLabel>
        <FormInput
          inputStyle={styles.textInput}
          keyboardType='email-address'
          autoCorrect={false}
          autoCapitalize="none"
          onChangeText={(text) => {
            this.emailAddrTextChange(text)
          }}
          defaultValue={this.state.auth.email}
        />
        <FormLabel>Password</FormLabel>
        <FormInput
          inputStyle={styles.textInput}
          secureTextEntry={true}
          autoCorrect={false}
          autoCapitalize="none"
          onChangeText={(text) => {
            this.updateAuthObj('password', text)
          }}
          defaultValue={this.state.auth.password}
        />
        <Button
          onPress={() => {
            this.submitLoginForm(this.state.auth)
          }}
          title="Submit"
          style={styles.button}
          color="#841584"
          accessibilityLabel="Learn more about this purple button"
          large
        ></Button>
        <Button
          onPress={this.dummyAcctLogin}
          title='Dummy Login'
          large
        />
      </View>
      )
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
    padding: 10
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  textInput: {
    height:50
  }
});

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    user: state.user.toJS(),
    firebaseState: state.firebaseState.toJS()
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);
