import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  ScrollView,
  View,
  Dimensions,
  TouchableOpacity,
  Image,
  Slider,
  BackAndroid
} from 'react-native';
import {
  Button,
  Grid, Row, Col, Icon
} from 'react-native-elements';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import actions from '../actions/index';
import { ActionCreators } from '../actions/index';
import CalendarPicker from 'react-native-calendar-picker';
import Moment from 'moment';
import utils from '../lib/utils';
const colors = utils.colors.midnightBlue;


class Calendar extends Component {
  constructor(props) {
    super(props);
    var date;
    if (this.props.date == undefined || this.props.date == null) {
      date = new Moment();
    } else {
      date = this.props.date
    }

    if (date.get('minutes') >= 30) {
      date.set('minutes', 30)
    } else {
      date.set('minutes', 0);
    }
    this.timesList = this.generateTimes();
    this.onDateChange = this.onDateChange.bind(this);
    this.onDonePress = this.onDonePress.bind(this);
    this.state = {
      date: date,
      selectedTime: this.findTimeKeyByDateTime(date)
    }
    BackAndroid.addEventListener('hardwareBackPress', this.onDonePress);
  };

  generateTimes() {
    var times = [];
    for (var i = 0; i < 48; i++) {
      if ( i == 0 || i == 24 ) {
        times[i] = "12:00";
      } else if ( i == 1 || i == 25 ) {
        times[i] = "12:30"
      } else if ( i%2 == 0) { // EVEN
        times[i] = (Math.floor(i/2) % 12)+ ":00"
      } else if ( i%2 == 1) { // ODD
        times[i] = (Math.floor(i/2) % 12) + ":30"
      }

      if (i < 24) {
        times[i] = times[i] + ' am'
      } else {
        times[i] = times[i] + ' pm'
      }
    }
    return times;
  }

  findTimeKeyByDateTime (dateTime) {
    return this.timesList.findIndex((time) => {
      return (time == dateTime.format('h:mm a'))
    });
  }

  addTimeToDate(timeIndex) {

    this.state.date.set('hours', Math.floor(timeIndex/2) % 24)
    this.state.date.set('minutes', 0)
    if (timeIndex%2 == 1) { // ODD
      this.state.date.set('minutes', 30)
    }

    this.setState({
      ...this.state,
      date: this.state.date
    }, () => {
      this.props.onDateChange(this.state.date);
    });
  }

  onDonePress () {
    this.props.onDonePress(this.state, this.props.key);
    return true;
  }

  onDateChange (date) {
    var m = Moment(date);

    m.set('hours', this.state.date.get('hours'));
    m.set('minutes', this.state.date.get('minutes'));
    this.setState({
      ...this.state,
      date: m
    }, function() {
      this.props.onDateChange(m);
    });
  }

  onTimeSelect (time) {
    this.setState({
      ...this.state,
      selectedTime: time
    });
  }

  render() {

    const calStyles = StyleSheet.create({
      container: {
        flex: 1,
      },
    });

    const cal = (
      <View style={calStyles.container}>
        <View style={{flex:.1, justifyContent:'flex-start', alignItems: 'flex-start'}}>
          <TouchableOpacity onPress={this.onDonePress}>
            <Icon
              color={colors.darkPrimary}
              size={30}
              name='chevron-left'
            />
          </TouchableOpacity>
        </View>
        <View style={{flex:.7}}>
          <CalendarPicker
            selectedDate={this.state.date.toDate()}
            onDateChange={this.onDateChange}
            screenWidth={Dimensions.get('window').width}
            selectedBackgroundColor={colors.darkPrimary}
            selectedDayColor={colors.secondary}
            textStyle={{color: colors.darkPrimary}}
          />
        </View>
        <View style={{flex: .5}}>
          <View style={{flex:.5, alignItems:'center'}}>
            <Text
              style={{
                color: colors.darkestPrimary,
                fontSize: 48,
                backgroundColor: 'transparent'
              }}
              >
              {this.timesList[this.state.selectedTime]}
            </Text>
          </View>
          <View style={{flex:1, marginRight: 20, marginLeft:20}}>
            <Slider
              maximumValue={47}
              minimumValue={0}
              onValueChange={(val) => {this.onTimeSelect(val)}}
              onSlidingComplete={(val) => {this.addTimeToDate(val)}}
              step={1}
              maximumTrackTintColor={colors.darkPrimary}
              minimumTrackTintColor={colors.secondary}
            />
          </View>
        </View>
        <View style={{flex:.2}}>
          <Button
            buttonStyle={{backgroundColor: colors.secondary}}
            title={this.state.date.format('MMM Do @ h:mm a')}
            onPress={this.onDonePress}/>
        </View>
      </View>
    )

    return (
      <Image source={require('../../images/blurred-background.jpg')} style={styles.backgroundImage}>
        <View style={styles.container}>
          {cal}
        </View>
      </Image>
    )
  };
};

const styles = StyleSheet.create({
  backgroundImage: {
    width: undefined,
    height: undefined,
    flex:1
  },
  container: {
    flex: 1,
    padding: 10
  },
  header: {
    color: colors.darkestPrimary,
    fontSize: 15
  },
  msg: {
    fontSize: 20,
    textAlign: 'center',
    color: '#b4bbc5'
  }
});

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    navigation: state.navigation.toJS()
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calendar);
