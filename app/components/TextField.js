import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TextInput
} from 'react-native';
import {
  Button,
  FormLabel,
  FormInput,
  Grid, Col, Row
} from 'react-native-elements';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../actions/index';
import utils from '../lib/utils';

const colors = utils.colors.midnightBlue;

class TextField extends Component {
  constructor(props) {
    super(props);

    // Will hold the ref of textinput value
    this.ref = '';

    // Default values for the textInput,
    // Will be replaced w/ any props passed
    this.state = {
      keyboardType: "default",
      placeholder: "",
      defaultValue: "",
      secureTextEntry: false,
      editable: true,
      autoCapitalize: "none",
      autoCorrect: false,
      autoFocus: false,
      placeholderTextColor: colors.darkerWhite,
      ref: this.ref
    }
    // replace any missing props w/ defaults
    this.state = Object.assign({}, this.state, this.props);
    this.onChangeText = this.onChangeText.bind(this);
  };

  onChangeText (val) {
    this.setState({value: val});
  }

  render() {
    return (
        <TextInput
          style={styles.container}
          onChangeText={this.onChangeText}
          keyboardType={this.state.keyboardType}
          placeholder={this.state.placeholder}
          defaultValue={this.state.defaultValue}
          secureTextEntry={this.state.secureTextEntry}
          editable={this.state.editable}
          placeholderTextColor={this.state.placeholderTextColor}
          autoCorrect={this.state.autoCorrect}
          autoCapitalize={this.state.autoCapitalize}
        />
    )
  };
};

const styles = StyleSheet.create({
  container: {
    height: 50,
    backgroundColor: colors.lightPrimary,
    padding: 10,
    borderColor: colors.lightSecondary,
    borderWidth: 2,
    borderRadius: 3,
    color: colors.darkWhite
  },
  textField: {
    height: 30,
    fontSize: 20,
    color: colors.lightestPrimary
  }
});

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    navigation: state.navigation.toJS(),
    user: state.user.toJS()
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(TextField);
