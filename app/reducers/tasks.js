import createReducer from '../lib/createReducer'
import { List, Map } from 'immutable'
import * as types from '../actions/types'

const uid = () => Math.random().toString(34).slice(2);

export const tasks = createReducer(List([]), {
  [types.ADD_TASK](state, action) {
    var task = Object.assign({id: uid()},
      action.payload
    );

    return state.push(Map(task));
  }
});
