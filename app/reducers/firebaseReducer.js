import createReducer from '../lib/createReducer'
import { List, Map } from 'immutable'
import * as types from '../actions/types'
import { fromJS } from 'immutable';

const init = fromJS({
  isLoggedIn: true,
  loading: false
});

export const firebaseState = createReducer(init, {
  [types.LOADING](state, action) {
    return state.set('loading', fromJS(action.isLoading));
  },

  [types.IS_LOGGED_IN](state, action) {
    return state.set('isLoggedIn', fromJS(action.isLoggedIn));
  }
});
