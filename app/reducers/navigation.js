import ReactNative from 'react-native';
const { NavigationExperimental, StatusBar} = ReactNative;
import * as types from '../actions/types'
import createReducer from '../lib/createReducer'
import LoginForm from '../components/LoginForm'
import { Map, List, fromJS } from 'immutable';

const init = fromJS({
  stack: [
    {
      sceneId: 'EditProfile',
      sceneProps: {
        'sunny': 'test'
      }
    }
  ],
  drawerOpen: false,
  scenes: [
    {
      sceneId: 'NewSession',
      title: 'Start a new session',
      showSceneHeader: false,
      showInSideMenu: true,
      icon: 'av-timer'
    },
    {
      sceneId: 'EditProfile',
      title: 'Your Profile',
      showSceneHeader: true,
      showInSideMenu: true,
      icon: 'account-circle'
    },
    {
      sceneId: 'Calendar',
      title: 'Calendar',
      showSceneHeader: false,
      showInSideMenu: false,
      icon: 'date_range'
    }
  ]
});


export const navigation = createReducer(init, {

  [types.NAVIGATION_FORWARD](state, action) {
    sceneFound = state.get('scenes').find((scene) => {
      return scene.get('sceneId') === action.sceneId
    });

    // don't change the scene because it's not found
    if (sceneFound === undefined) {
      return state;
    }

    return state.set('stack', state.get('stack').push(
        {
          sceneId: action.sceneId,
          sceneProps: action.sceneProps
        }
      )
    );
  },

  [types.NAVIGATION_BACK](state, action) {
    // Don't remove last scene
    if (state.get('stack').size > 1) {
      state = state.set('stack', state.get('stack').pop());
    }
    return state;
  },

  [types.TOGGLE_DRAWER](state, action) {
    drawerOpen = !state.get('drawerOpen');
    if (action.open != null) {
      drawerOpen = action.open;
    }

    return state.set('drawerOpen', drawerOpen);
  },

  [types.CLOSE_DRAWER](state, action) {
    return state.set('drawerOpen', false);
  }

});
