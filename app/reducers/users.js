import createReducer from '../lib/createReducer'
import { List, Map } from 'immutable'
import * as types from '../actions/types'
import { fromJS } from 'immutable';

const init = fromJS({
  profile: {
    firstName: '',
    lastName: '',
    email: ''
  }
});

export const user = createReducer(init, {
  [types.SET_FIREBASE_USER](state, action) {
    return state;
  },
  [types.SET_USER_PROFILE](state, action) {
    return state.set('profile', fromJS(action.payload));
  }
});
