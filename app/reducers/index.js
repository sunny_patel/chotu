import * as usersReducer from './users'
import * as tasksReducer from './tasks'
import * as navigationReducer from './navigation'
import * as firebaseReducer from './firebaseReducer'

// import { combineReducers } from 'redux-immutable'
import { combineReducers } from 'redux'

export default combineReducers(Object.assign(
  navigationReducer,
  usersReducer,
  tasksReducer,
  firebaseReducer
));
