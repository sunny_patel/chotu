import * as firebase from 'firebase';
import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  StatusBar,
  BackAndroid
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../actions/index';
import WelcomeContainer from './WelcomeContainer';
import Loading from '../components/Loading';
import MainContainer from './MainContainer';

class AppContainer extends Component {

  constructor(props, context) {
    super(props, context);

    this.handleBackPress = this.handleBackPress.bind(this);
    BackAndroid.addEventListener('hardwareBackPress', this.handleBackPress);
  }

  handleBackPress() {
    if (this.props.navigation.stack.length == 1) {
      BackAndroid.exitApp(1);
      return false;
    } else {
      this.props.navigateBack();
      return true;
    }

  }

  render() {
    if (this.props.firebaseState.loading) {
      return (
        <Loading/>
      )
    }
    else if (!this.props.firebaseState.isLoggedIn) {
      return (
        <WelcomeContainer/>
      )
    }
    else {
      return (
        <View style={{flex:1}}>
          <StatusBar
            backgroundColor="#1A237E"
            barStyle="light-content"
            animated={true}
            showHideTransition='slide'
            hidden={true}
          />
          <MainContainer/>
        </View>
      );
    }
  }
}


function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    user: state.user.toJS(),
    navigation: state.navigation.toJS(),
    firebaseState: state.firebaseState.toJS()
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(AppContainer);
