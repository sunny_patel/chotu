import * as firebase from 'firebase';
import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet,
  Animated,
  LayoutAnimation,
  UIManager
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { ActionCreators } from '../actions/index';
import WelcomeContainer from './WelcomeContainer';
import Loading from '../components/Loading';
import NewSession from '../components/NewSession/NewSession';
import SceneHeader from './DrawerContainer/SceneHeader';
import ActiveSession from '../components/ActiveSession';
import EditProfile from '../components/EditProfile';
import MainContainer from './MainContainer';
import Calendar from '../components/Calendar';
import utils from '../lib/utils';

const colors = utils.colors.midnightBlue;
UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true);

class SceneContainer extends Component {

  constructor(props, context) {
    super(props, context);
    this.state = {
      bounceValue: new Animated.Value(0),
      sceneStyle: {
        opacity: 100
      }
    };
    this.fadeIn = this.fadeIn.bind(this);
    this.fadeOut = this.fadeOut.bind(this);
  }

  fadeIn() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.state.sceneStyle = {
      opacity: 100
    }
  }

  fadeOut() {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    this.state.sceneStyle = {
      opacity: 0
    }
  }

  render() {
    const sceneStyle = [styles.scene, this.state.sceneStyle];

    const sceneToRender = function(currentScene, sceneProps) {

      switch(currentScene.toLowerCase()) {
        case 'newsession':
          return <NewSession style={styles.scene} {...sceneProps}/>
        break;
        case 'activesession':
          return <ActiveSession style={styles.scene} {...sceneProps}/>
        break;
        case 'editprofile':
          return <EditProfile style={styles.scene} {...sceneProps}/>
        case 'calendar':
          return <Calendar style={styles.scene} {...sceneProps}/>
        default:
          return null
        break;
      }
    }

    return (
      <View style={styles.container}>
        <View style={sceneStyle}>
          {sceneToRender(this.props.navigation.stack[this.props.navigation.stack.length-1].sceneId,
                          this.props.navigation.stack[this.props.navigation.stack.length-1].sceneProps)}

        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  sceneHeader: {
    flex: .1,
  },
  scene: {
    flex: .9,
  },
  msg: {
    fontSize: 20,
    textAlign: 'center',
    color: '#fafafa'
  }
});

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    navigation: state.navigation.toJS()
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(SceneContainer);
