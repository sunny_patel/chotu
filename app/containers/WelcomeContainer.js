import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TextInput,
  Button,
  Keyboard
} from 'react-native';
import KeyboardSpacer from 'react-native-keyboard-spacer';
import actions from '../actions';
import utils from '../lib/utils';
import SignupForm from '../components/SignupForm';
import LoginForm from '../components/LoginForm';
import Swiper from 'react-native-swiper';

class WelcomeContainer extends Component {
  constructor(props) {
    super(props);
  };

  render() {
    return (
      <Swiper style={styles.wrapper} showsButtons={false}>
        <View style={styles.slide1}>
         <LoginForm/>
         <KeyboardSpacer/>
        </View>
        <View style={styles.slide2}>
         <SignupForm/>
         <KeyboardSpacer/>
        </View>
      </Swiper>
    )
  };
};

const styles = StyleSheet.create({
  wrapper: {
  },
  slide1: {
    flex: 1,
  },
  slide2: {
    flex: 1
  }
});

export default WelcomeContainer;
