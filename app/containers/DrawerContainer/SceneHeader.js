import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  TextInput,
  Button
} from 'react-native';
import { Icon } from 'react-native-elements';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import actions from '../../actions/index';
import { ActionCreators } from '../../actions/index';
import utils from '../../lib/utils';

const colors = utils.colors.midnightBlue;

class SceneHeader extends Component {
  constructor(props) {
    super(props);
    this.onHamburgerPress = this.onHamburgerPress.bind(this);
    this.getSceneTitle = this.getSceneTitle.bind(this);

    if (this.props.sceneHeaderColor == undefined) {
      this.props.sceneHeaderColor = colors.lightWhite
    }

    this.state = {
      sceneTitle: this.props.sceneTitle,
      sceneHeaderColor: this.props.sceneHeaderColor
    }
  };

  onHamburgerPress() {
    this.props.navigation.navigate('DrawerOpen')
  }

  getSceneTitle(scene) {
    if (this.state.sceneTitle == undefined) {
      return scene.title
    } else {
      return this.state.sceneTitle
    }

  }

  render() {
    return (
    <View style={[styles.container, {backgroundColor: this.state.sceneHeaderColor}]}>
      <TouchableHighlight style={styles.hamburger} onPress={this.onHamburgerPress} underlayColor={colors.lightWhite}>
        <View>
          <Icon name='menu' color={colors.darkestPrimary}/>
        </View>
      </TouchableHighlight>
      <View style={styles.msgContainer}>
        <Text style={styles.msgText}>{this.getSceneTitle(utils.lastEl(this.props.myNavigation.stack))}</Text>
      </View>
    </View>
    )
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  hamburger: {
    flex: .20,
    justifyContent: 'center'
  },
  msgContainer: {
    flex:.80,
    justifyContent: 'center',
    padding: 30,
    paddingLeft: 10
  },
  msgText: {
    color: colors.darkestPrimary,
    fontSize: 20
  }
});


function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    myNavigation: state.navigation.toJS()
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SceneHeader);
