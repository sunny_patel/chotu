import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight,
  TextInput,
  Button,
  Switch
} from 'react-native';
import {
  List,
  ListItem,
  Grid, Row, Col,

} from 'react-native-elements';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux';
import actions from '../../actions/index';
import { ActionCreators } from '../../actions/index';
import utils from '../../lib/utils';
const colors = utils.colors.midnightBlue;

class SideMenuContent extends Component {
  constructor(props) {
    super(props);
  }

  onItemPressed(item) {
    this.props.navigateForward(item.sceneId);
    this.props.closeDrawer();
  }

  render() {
    var listItems = [];
    scenes = this.props.navigation.scenes.filter((scene) => {
      return scene.showInSideMenu
    });
    for (var i=0; i < scenes.length; i++) {
      var item = this.props.navigation.scenes[i];

      listItems.push(<ListItem
        onPress={this.onItemPressed.bind(this, item)}
        key={i}
        title={item.title}
        leftIcon={{name: item.icon}}
        underlayColor={colors.lightPrimary}
        containerStyle={styles.menuItemStyle}
        titleStyle={styles.menuItemTitle}
      />);
    }


    return (
      <View style={styles.container}>
        <List containerStyle={styles.listContainer}>
          {listItems}
        </List>
      </View>
    )
  };
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: colors.darkPrimary,
  },
  listContainer: {
    borderColor: colors.darkPrimary,
    borderWidth: 0,
    borderBottomWidth: 0
  },
  msg: {
    fontSize: 20,
    textAlign: 'center',
    color: colors.darkWhite
  },
  menuItemStyle: {
    backgroundColor: colors.darkPrimary,
    borderBottomWidth: 2,
    borderBottomColor: colors.darkestPrimary
  },
  menuItemTitle: {
    color: colors.darkWhite
  }
});

function mapDispatchToProps (dispatch) {
  return bindActionCreators(ActionCreators, dispatch);
};

function mapStateToProps (state) {
  return {
    navigation: state.navigation.toJS()
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(SideMenuContent);
