import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableHighlight,
  StyleSheet
} from 'react-native';
import NewSession from '../components/NewSession/NewSession';
import ActiveSession from '../components/ActiveSession';
import EditProfile from '../components/EditProfile';
import { DrawerNavigator } from 'react-navigation';

const MyApp = DrawerNavigator({
  EditProfile: {
    screen: EditProfile,
  },
  NewSession: {
    screen: NewSession,
  },
});

export default class MainContainer extends Component {

  render() {
    return (
      <MyApp/>
    )
  }
}
