import * as firebase from 'firebase';

export default class FirebaseApi {
  constructor () {
    console.debug('THIS SHOULD ONLY HAPPEN ONCE');
    // Initialize Firebase
    const firebaseConfig = {
      apiKey: "AIzaSyD0DK7XFNv3qtGkWhWK7VdySVm4Hq9ktaw",
      authDomain: "chotu-a21e6.firebaseapp.com",
      databaseURL: "https://chotu-a21e6.firebaseio.com",
      storageBucket: "chotu-a21e6.appspot.com",
      messagingSenderId: "382236783014"
    };
    this.handle = firebase.initializeApp(firebaseConfig);
  }
}
