import colors from '../configs/colors';

var utils = {
  colors,
  validEmail: function(email, opts={}) {
    return email.indexOf('@') > -1
  },
  lastEl: function(arr=[]) {
    if (arr.length == undefined)
      return null;
    return arr[arr.length-1];
  }
}

module.exports = utils;
