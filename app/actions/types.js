// Recipes
export const SET_SEARCHED_RECIPES = 'SET_SEARCHED_RECIPES'

export const LOADING = 'LOADING';
export const DONE_LOADING = 'DONE_LOADING';
// Navigation
export const SET_TAB = 'SET_TAB'

export const NAVIGATION_FORWARD = 'NAVIGATION_FORWARD'
export const NAVIGATION_BACK = 'NAVIGATION_BACK'
export const ADD_TASK = 'ADD_TASK';

export const CREATE_USER = 'CREATE_USER'

export const SET_FIREBASE_USER = 'SET_FIREBASE_USER';
export const SET_USER_PROFILE = 'SET_USER_PROFILE';

export const AUTHENICATE_USER = 'AUTHENICATE_USER';
export const IS_LOGGED_IN = "IS_LOGGED_IN";
export const TOGGLE_DRAWER = 'TOGGLE_DRAWER';
export const CLOSE_DRAWER = 'CLOSE_DRAWER';
