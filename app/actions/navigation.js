import * as types from './types'
import ReactNative from 'react-native'

export function navigate(action) {
  return (dispatch, getState) => {
    dispatch(navigateForward(action))
  }
}

export function navigateForward(sceneId, sceneProps) {

  return {
    type: types.NAVIGATION_FORWARD,
    sceneId,
    sceneProps
  }
}

export function navigateBack() {
  return {
    type: types.NAVIGATION_BACK
  }
}

export function toggleDrawer(open = null) {
  return {
    type: types.TOGGLE_DRAWER,
    open
  }
}

export function closeDrawer() {
  return {
    type: types.CLOSE_DRAWER
  }
}
