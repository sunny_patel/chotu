import * as types from './types';
import * as firebase from 'firebase';
import _ from 'underscore';
import * as UserActions from './userActions';

export function createFirebaseUser(user) {
  return(dispatch, getState) => {
    dispatch(loading(true));
    return firebase.auth().createUserWithEmailAndPassword(user.email,user.password)
    .catch((error) => {
      if (error != {}) {
        console.error("ACTION: Firebase CreateUser -> error creating new user: " + JSON.stringify(error));
      }
    })
    .then((currentUser) => {
      console.log("ACTION: Firebase CreateUser -> successfully created user: " + JSON.stringify(currentUser));
      user.uid = currentUser.uid;
      // save the remaining fields
      dispatch(saveUserProfileOnFirebase(user));
      dispatch(loading(false));
    });
  }
};

export function saveUserProfileOnFirebase(userProfile) {
  return (dispatch, getState) => {
    // don't save password in profile
    if (userProfile['password'] != undefined ||
        userProfile['password'] != null)
        delete userProfile['password'];

    var refId = userProfile.email.replace('.', '').replace('@', '');
    var userRef =  firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/profile');
    dispatch(loading(true));
    return userRef.set(userProfile)
    .then(() => {
      console.log("ACTION : Firebase SaveUserProfileOnFirebase -> Firebase user profile update success " + JSON.stringify(userProfile));
      dispatch(loading(false))
      dispatch(UserActions.setUserProfile(userProfile));
    })
    .catch((err) => {
      if (!_.isEqual(err, {})) {
        console.error("ACTION : Firebase SaveUserProfileOnFirebase -> Firebase user profile update failed " + JSON.stringify(err));
      }
    });
  }
};

export function authenicateUser(auth) {
  return(dispatch) => {
    dispatch(loading(true))
    return firebase.auth().signInWithEmailAndPassword(auth.email, auth.password)
    .catch((error) => {
      if (error != {}) {
        console.error("ACTION: Firebase AuthenicateUser -> error authenicating user: " + JSON.stringify(error));
      }
    })
    .then((user) => {
      console.log("ACTION: Firebase AuthenicateUser -> successfully authenicated user " + JSON.stringify(firebase.auth().currentUser));
      dispatch(getUserProfileFromFB());
      dispatch(checkIfLoggedIn());
      dispatch(loading(false));
    });
  }
};

export function getUserProfileFromFB() {
  return(dispatch) => {

    console.log("ACTION: Firebase getUserProfileFromFB -> Pulling Data...");

    var userProfileRef = firebase.database().ref('users/' + firebase.auth().currentUser.uid + '/profile');
    var handleDataUpdate = (data) => {
      console.log("ACTION: Firebase getUserProfileFromFB -> Received Data... " + JSON.stringify(data.val()));
      dispatch(UserActions.setUserProfile(data.val()));
    }
    handleDataUpdate = handleDataUpdate.bind(this);

    userProfileRef.on('value', handleDataUpdate);
  }
};

export function checkIfLoggedIn() {
  isLoggedIn = (firebase.auth().currentUser != null) ? true : false;
  return {
    type: types.IS_LOGGED_IN,
    isLoggedIn: isLoggedIn
  }
};

export function loading(isLoading) {
  return {
    type: types.LOADING,
    isLoading: isLoading
  }
};
