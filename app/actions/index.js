import * as FirebaseActions from './firebaseActions';
import * as UserActions from './userActions';
import * as TaskActions from './taskActions';
import * as NavigationActions from './navigation';

export const ActionCreators = Object.assign({},
  NavigationActions,
  UserActions,
  TaskActions,
  FirebaseActions
);
