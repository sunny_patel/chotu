import * as types from './types';
import * as firebase from 'firebase';

export function setFirebaseUser(currentUser) {
  return {
    type: types.SET_FIREBASE_USER,
    payload: currentUser,
  }
}

export function setUserProfile(userProfile) {
  return {
    type: types.SET_USER_PROFILE,
    payload: userProfile
  }
}
